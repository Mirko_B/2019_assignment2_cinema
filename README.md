Assignment 2

Gruppo formato da Bertini Simone, Luca Amorosia, Mirko Brigatti.

L'applicazione da noi presa in esame per il secondo assignment del corso è una app di una catena di cinema. Le tecniche di elicitazione sono intervista + questionario. 
Allo stato attuale del progetto abbiamo prodotto delle bozze per questionario e intervista con una spiegazione preliminare di come sono raggruppate e cosa vogliono scoprire durante la loro somministrazione. Inoltre è stato definito lo schema generale della relazione finale con i vari capitoli.

Il link della repo è https://gitlab.com/Mirko_B/2019_assignment2_cinema