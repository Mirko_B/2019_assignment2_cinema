Prenotazione
-  Quali dati vengono richiesti per effettuare una prenotazione?

-  Come gestisce una prenotazione annullata?

-  Che metodi di pagamento sono accettati al momento alle casse ?

-  E' possibile al momento prenotare un biglietto e pagarlo successivamente in loco ? 

5)  Che strumenti ha a disposizione per il controllo biglietti? E per la convalida?


Altri servizi 

-  Come è organizzata l'area bar ? Utilizzate già strumenti digitalizzati per la gestione ordini cibo e bevande ?

-  In quali occasioni viene utilizzata la carta fedeltà e che strumenti usate per interfacciarvi con essa ? 

-  E' presente una figura dedicata alla risposta telefonica/telematica delle domande dei clienti? Se sì come funziona il processo di risposta? 


(-  Secondo lei sarebbe utile all’interno dell’applicazione un’area riservata al personale?)



L'intervista ai dipendenti viene fatta per ... (aspetti applicativi delle funzionalità e interfacciamento con sistema preesistente)

La sezione ...-... è incentrata sulla prenotazione dei biglietti, la funzione più importante dell'applicazione. Le domande poste vogliano mostrare come vengono gestite attualmente le prenotazioni in modo da renderle consistenti con quanto offerto nell'app. Inoltre domande come la 5) aiutano a delineare quali tecnologia siano più indicate per digitalizzare il biglietto (QRCode, codici a barre, ...)

La sezione ...-... indaga sugli altri servizi offerti dal cinema quali area bar, utilizzo della carta fedeltà e centralino che saranno toccate da alcune funzioni dell'applicazione. Altre funzioni dell'applicazione quali Trovare Cinema e Recensioni vengono invece escluse da queste interviste ai dipendenti, in quanto non impattano sul loro lavoro quotidiano e rimangono appannaggio dei servizi web.

