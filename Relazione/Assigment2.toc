\contentsline {section}{\numberline {1}Introduzione}{2}{section.1}
\contentsline {section}{\numberline {2}Descrizione dell' applicazione}{2}{section.2}
\contentsline {section}{\numberline {3}Identificazione degli stakeholders}{2}{section.3}
\contentsline {subsection}{\numberline {3.1}Manager}{2}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Impiegati}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Clienti}{3}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Abituali}{3}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}Occasionali}{3}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}Gruppi Famiglia}{3}{subsubsection.3.3.3}
\contentsline {section}{\numberline {4}Strategia di elicitazione}{3}{section.4}
\contentsline {subsection}{\numberline {4.1}StakeHolder-Driven}{3}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Artifact-Driven}{3}{subsection.4.2}
\contentsline {section}{\numberline {5}Interviste}{4}{section.5}
\contentsline {subsection}{\numberline {5.1}Manager}{4}{subsection.5.1}
\contentsline {paragraph}{Sezione Conoscitiva}{4}{section*.2}
\contentsline {paragraph}{Caratteristiche Generali}{4}{section*.3}
\contentsline {paragraph}{Funzionalit\IeC {\`a}}{4}{section*.4}
\contentsline {subsection}{\numberline {5.2}Dipendenti}{5}{subsection.5.2}
\contentsline {paragraph}{Prenotazione}{5}{section*.5}
\contentsline {paragraph}{Altri Servizi}{5}{section*.6}
\contentsline {section}{\numberline {6}Questionario}{5}{section.6}
\contentsline {subsection}{\numberline {6.1}Sezione Domande}{5}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Generale}{5}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Carta Fedelt\IeC {\`a}}{6}{subsubsection.6.1.2}
\contentsline {subsubsection}{\numberline {6.1.3}Prenotazione e Acquisto}{6}{subsubsection.6.1.3}
\contentsline {subsubsection}{\numberline {6.1.4}Trovare e Contattare Cinema}{6}{subsubsection.6.1.4}
\contentsline {subsubsection}{\numberline {6.1.5}Recensioni}{6}{subsubsection.6.1.5}
\contentsline {subsubsection}{\numberline {6.1.6}Applicazione}{6}{subsubsection.6.1.6}
\contentsline {subsection}{\numberline {6.2}Il Questionario}{6}{subsection.6.2}
\contentsline {paragraph}{Sezione Generale}{6}{section*.7}
\contentsline {paragraph}{Carta Fedelt\IeC {\`a}}{7}{section*.8}
\contentsline {paragraph}{Prenotazione e Acquisto}{7}{section*.9}
\contentsline {paragraph}{Trovare e Contattare Cinema}{8}{section*.10}
\contentsline {paragraph}{Recensioni}{9}{section*.11}
\contentsline {paragraph}{Applicazione}{9}{section*.12}
\contentsline {section}{\numberline {7}Dati raccolti}{10}{section.7}
\contentsline {section}{\numberline {8}Conclusioni}{13}{section.8}
\contentsline {paragraph}{Carta Fedelt\IeC {\`a}}{13}{section*.13}
\contentsline {paragraph}{Prenotazione Biglietti}{13}{section*.14}
\contentsline {paragraph}{Trovare e Contattare Cinema}{14}{section*.15}
\contentsline {paragraph}{Recensioni}{14}{section*.16}
\contentsline {paragraph}{Varie}{14}{section*.17}
